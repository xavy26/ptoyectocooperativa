var express = require('express');
var router = express.Router();
var passport   = require('passport');
var needle = require('needle');

let url_server = 'http://localhost:8000/api/v1/';

/* USER */
router.get('/', function(req, res, next) {
    req.flash;
    res.render('template', {usuario: "", title: "login cooperativa", template: "user/login", mensaje: req.flash('mensaje') });
});
router.get('/home', function(req, res, next) {
    req.flash;
    if(!req.isAuthenticated()){
        req.flash('mensaje', 'No puedes acceder sin Iniciar Sesión.');
        res.redirect('/');
    }
    res.render('template', {usuario: req.user.name,  title: "home cooperativa", template: "user/home", mensaje: req.flash('mensaje') });
});
/* AUTH */
router.post('/auth', passport.authenticate('local-signin',  { successRedirect: '/home', failureRedirect: '/' }));
router.get('/logout', function(req, res, next){
    req.logout();
    req.session.destroy(function (err) {
        res.redirect('/');
    });
});
/* CLIENTE */
router.get('/cliente', function(req, res, next) {
    req.flash;
    if(!req.isAuthenticated()){
        req.flash('mensaje', 'No puedes acceder sin Iniciar Sesión.');
        res.redirect('/');
    }
    needle.get(url_server + 'cliente/', function(err, response){
        if(!err && response.statusCode == 200){
            if(req.user == undefined){
                req.flash('mensaje', 'No puedes acceder sin Iniciar Sesión.');
                res.redirect('/');
            }
            res.render('template', {clientes: response.body,
                                    usuario: req.user.name, title: "Clientes cooperativa", template: "cliente/admin", mensaje: req.flash('mensaje') });
        } else {
            req.flash('mensaje', "Error al conectar con el Servicio");
            res.redirect('/home');
        }
    });
});
router.get('/cliente/nuevo', function(req, res, next) {
    req.flash;
    if(!req.isAuthenticated()){
        req.flash('mensaje', 'No puedes acceder sin Iniciar Sesión.');
        res.redirect('/');
    }
    res.render('template', {usuario: req.user.name, title: "Clientes cooperativa", template: "./cliente/nuevo", mensaje: req.flash('mensaje') });
});
router.post('/cliente/registrar', function(req, res, next) {
    var data = {
        cedula : req.body.cedula,
        nombres : req.body.nombre,
        apellidos : req.body.apellido,
        genero : req.body.genero,
        estadoCivil : req.body.estadoCivil,
        fechaNacimiento : req.body.nacimiento,
        correo : req.body.correo,
        telefono : req.body.telefono,
        celular : req.body.celular,
        direccion: req.body.direccion
    };
    needle('post', url_server + 'cliente/', data, { json: true })
        .then(function(response){
        console.log(response.body);
        if(response.body.cliente_id){
            let nro_cuentas=0;
            needle.get(url_server + 'cuenta/', function(err, respon){
                if(!err && respon.statusCode == 200){
                    nro_cuentas = respon.body.lenght + 1;
                }
            });

            needle('post', url_server + 'cuenta/', {
                numero : response.body.cedula+""+response.body.cliente_id + "" + nro_cuentas,
                saldo : 0,
                tipoCuenta : req.body.tipoCuenta,
                cliente: response.body.cliente_id,
                banco: 0
            }, { json: true })
                .then(function(resp){
                console.log(resp.body);
                if(resp.body.cuenta_id){
                    req.flash('mensaje', "Registro Correcto");
                }else{
                    req.flash('mensaje', "Error de Registro de Cuenta");
                }
                res.redirect("/cliente");
            }).catch(function(err){
                req.flash('mensaje', "Error al conectar con el Servicio");
                console.log(err);
                res.redirect("/cliente/nuevo");
            });  
        }else{
            req.flash('mensaje', "Error de Registro de Cliente");
        }
        res.redirect("/cliente");
    }).catch(function(err){
        req.flash('mensaje', "Error al conectar con el Servicio");
        console.log(err);
        res.redirect("/cliente/nuevo");
    });    
});
/* CUENTA */
router.get('/cuenta', function(req, res, next) {
    req.flash;
    if(!req.isAuthenticated()){
        req.flash('mensaje', 'No puedes acceder sin Iniciar Sesión.');
        res.redirect('/');
    }
    needle.get(url_server + 'cuenta/', function(err, response){
        if(!err && response.statusCode == 200){
            if(req.user == undefined){
                req.flash('mensaje', 'No puedes acceder sin Iniciar Sesión.');
                res.redirect('/');
            }
            needle.get(url_server + 'cliente/', function(err, respon){
                if(!err && respon.statusCode == 200){
                    if(req.user == undefined){
                        req.flash('mensaje', 'No puedes acceder sin Iniciar Sesión.');
                        res.redirect('/');
                    }
                    res.render('template', {cuentas: response.body, clientes: respon.body,
                                            usuario: req.user.name, title: "Cuentas cooperativa", template: "cuenta/admin", mensaje: req.flash('mensaje') });
                } else {
                    req.flash('mensaje', "Error al conectar con el Servicio");
                    res.redirect('/home');
                }
            });
        } else {
            req.flash('mensaje', "Error al conectar con el Servicio");
            res.redirect('/home');
        }
    });
});
router.get('/cuenta/nuevo', function(req, res, next) {
    req.flash;
    if(!req.isAuthenticated()){
        req.flash('mensaje', 'No puedes acceder sin Iniciar Sesión.');
        res.redirect('/');
    }
    res.render('template', {usuario: req.user.name, title: "Clientes cooperativa", template: "./cuenta/nuevo", mensaje: req.flash('mensaje') });
});
router.post('/cuenta/registrar', function(req, res, next) {
    needle.get(url_server + 'cliente/' + req.body.cliente, function(err, response){
        if(!err && response.statusCode == 200){
            let nro_cuentas=0;
            needle.get(url_server + 'cuenta/', function(err, respon){
                if(!err && respon.statusCode == 200){
                    nro_cuentas = respon.body.lenght + 1;
                    needle('post', url_server + 'cuenta/', {
                        numero : response.body.cedula+""+response.body.cliente_id + "" + nro_cuentas,
                        saldo : 0,
                        tipoCuenta : req.body.tipoCuenta,
                        cliente: response.body.cliente_id,
                        banco: 0
                    }, { json: true })
                        .then(function(resp){
                        console.log(resp.body);
                        if(resp.body.cuenta_id){
                            req.flash('mensaje', "Registro Correcto");
                        }else{
                            req.flash('mensaje', "Error de Registro de Cuenta");
                        }
                        res.redirect("/cliente");
                    }).catch(function(err){
                        req.flash('mensaje', "Error al conectar con el Servicio");
                        console.log(err);
                        res.redirect("/cliente/nuevo");
                    }); 
                }
            });
        }
    });   
});

module.exports = router;
