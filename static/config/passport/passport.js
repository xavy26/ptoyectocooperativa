var needle = require('needle');
var bCrypt = require('bcrypt-nodejs');

module.exports = function (passport) {

    let url_server = 'http://localhost:8000/api/v1/';
    var LocalStrategy = require('passport-local').Strategy;

    passport.serializeUser(function (account, done) {
        done(null, account.usuario_id);
    });

    // used to deserialize the user
    passport.deserializeUser(function (id, done) {

        needle.get(url_server + 'usuario/' + id, function(err, res){
            if(!err && res.statusCode == 200){
                var userinfo = {
                    account: res.body.usuario_id,
                    estado: res.body.estado,
                    email: res.body.correo,
                    name: res.body.nombre + " " + res.body.apellido
                };
                done(null, userinfo);
            } else {
                done(err, null);
            }
        });
    });
    //inicio de sesion
    passport.use('local-signin', new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'pass',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (req, email, password, done) {
            if(!email && !password){
                req.flash('mensaje','Debe Ingresar Todos los datos.');
                return done(null, false, {
                    message: 'Debe Ingresar Todos los datos'
                });
            }
            var emailRegEx = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
            if(!emailRegEx.test(email)){
                req.flash('mensaje','El Correo Ingresado es Incorrecto.');
                return done(null, false, {
                    message: 'El Correo Ingresado es Incorrecto'
                });
            }
            // var isValidPassword = function (userpass, password) {
            //     return bCrypt.compareSync(password, userpass);
            // }
            needle.get(url_server + 'usuario/', function(err, res){
                if(!err && res.statusCode == 200){
                    console.log(res.body);
                    for (var i = 0; i < res.body.length; i++) {
                        let user = res.body[i];
                        if (user.correo!=email) {
                            req.flash('mensaje','El Correo no existe.');
                            return done(null, false, {message: 'Correo no existe'});
                        }
                        if(!user.estado){
                            req.flash('mensaje','Tu Cuenta a sido Bloqueada');
                            return done(null, false, {message: 'Por favor verifica tu correo.'});
                        }
                        if(user.clave!=password){
                            req.flash('mensaje','Clave Incorrecta');
                            return done(null, false, {message: 'Clave Incorrecta'});
                        }
                        req.flash('mensaje', 'Bienvenido '+user.nombre + " " + user.apellido);
                        var userinfo = user;
                        return done(null, userinfo);
                    }
                } else {
                    console.log("Error:", err);
                    req.flash('mensaje','Cuenta erronea.');
                    return done(null, false, {message: 'Cuenta erronea'});
                }
            });
        }
    ));
}
