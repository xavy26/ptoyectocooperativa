# Generated by Django 2.1.5 on 2019-01-06 23:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('modelos', '0002_auto_20190106_1723'),
    ]

    operations = [
        migrations.AddField(
            model_name='banco',
            name='estado',
            field=models.BooleanField(default=True),
        ),
    ]
