from django.db import models

# Create your models here.
class Banco(models.Model):
    banco_id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=25)
    direccion = models.CharField(max_length=225)
    telefono = models.CharField(max_length=10)
    correo = models.EmailField(max_length=200)
    estado = models.BooleanField(null = False, default = True)

    def __str__(self):
        return self.nombre

class Usuario(models.Model):
    usuario_id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=60, null = False)
    apellido = models.CharField(max_length=60, null = False)
    correo = models.EmailField(max_length=200, null = False)
    clave = models.TextField(null = False)
    estado = models.BooleanField(null = False, default = True)

    def __str__(self):
        return self.nombre + " " + self.apellido

class Cliente(models.Model):

    listaGenero = (
        ('f', 'Femenino'),
        ('m', 'Masculino'),
    )
    listaEstadoCivil = (
        ('soltero', 'Soltero'),
        ('casado', 'Casado'),
        ('viudo', 'Viudo'),
        ('divorciado', 'Divorciado'),
        ('union', 'Union Libre'),
    )

    cliente_id = models.AutoField(primary_key=True)
    cedula = models.CharField(unique=True, max_length=10, null = False)
    nombres = models.CharField(max_length=70, null = False)
    apellidos = models.CharField(max_length=70, null = False)
    genero = models.CharField(max_length=15, choices = listaGenero, null = False)
    estadoCivil = models.CharField(max_length=15, choices = listaEstadoCivil, null = False)
    fechaNacimiento = models.DateField(auto_now = False, auto_now_add = False, null = False)
    correo = models.EmailField(unique=True, max_length=100, null = False)
    telefono = models.CharField(max_length=15, null = False)
    celular = models.CharField(max_length=15, null = False)
    direccion = models.TextField(null = False)
    #uuid = models.UUIDField(default=uuid.uuid4, editable=False)

    def __str__(self):
        return self.cedula

class Cuenta(models.Model):

    listaTipo = (
        ('corriente', 'Corriente'),
        ('ahorros', 'Ahorro'),
        ('basica', 'Básica'),
        ('nomina', 'Nómina'),
        ('valores', 'Valores'),
        ('juvenil', 'Juvenil'),
        ('programado', 'Ahorro Programado'),
        ('euros', 'Ahorro en Euros'),
    )
    cuenta_id = models.AutoField(primary_key=True)
    numero = models.CharField(max_length=20, unique=True, null = False)
    estado = models.BooleanField(null = False, default = True)
    fechaApertura = models.DateTimeField(auto_now_add = True, null = False)
    saldo = models.DecimalField(max_digits=10, decimal_places=3, null = False)
    tipoCuenta = models.CharField(max_length=30, choices = listaTipo, null = False)
    cliente = models.ForeignKey(
        'Cliente',
        on_delete=models.CASCADE,
    )
    banco = models.ForeignKey(
        'Banco',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.numero

class Transaccion(models.Model):

    listaTipoTransaccion = (
        ('retiro', 'Retiro'),
        ('deposito', 'Depósito'),
        ('transferencia', 'Transferencia'),
        ('prestamo', 'Pago de Prestamo'),
        ('nomina', 'Pagos de Nómina'),
        ('pensiones', 'Pagos de Pensiones'),
        ('dividendos', 'Dividendos'),
        ('reembolsoGastos', 'Reembolso de Gastos'),
        ('pagoProveedores', 'Reembolso de Gastos'),
        ('transferencia', 'Traslado de efectivo entre entidades bancarias'),
        ('seguros', 'Pago de Seguros'),
        ('iess', 'Pago del IESS'),
        ('hipotecas', 'Pago de Hipotecas'),
        ('serviciosBasico', 'Pago de Servicios Básicos'),
        ('tvCable', 'Cuentas de televisión por cable'),
        ('celular', 'Cuentas de celular'),
        ('online', 'Compras por Internet'),
        ('administracion', 'Servicio de Administración'),
        ('futuros', 'Pagos Futuros'),
    )
    transaccion_id = models.AutoField(primary_key=True)
    fecha = models.DateTimeField(auto_now_add = True, null = False)
    tipo = models.CharField(max_length=30, choices = listaTipoTransaccion, null = False)
    valor = models.DecimalField(max_digits=10, decimal_places=3, null = False)
    descripcion = models.TextField(null = False)
    cuenta = models.ForeignKey(
        'Cuenta',
        on_delete=models.CASCADE,
    )
    usuario = models.ForeignKey(
        'Usuario',
        on_delete=models.CASCADE,
        default = -1,
    )
