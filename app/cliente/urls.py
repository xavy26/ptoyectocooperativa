from django.urls import path

from app.cliente.views import ClienteView, SingleClienteView


app_name = "cliente"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    path('/', ClienteView.as_view()),
    path('/<int:pk>', SingleClienteView.as_view()),
]
