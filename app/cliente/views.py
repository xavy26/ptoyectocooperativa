from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from app.modelos.models import Cliente
from app.cliente.serializers import ClienteSerializer

# Create your views here.

class ClienteView(ListCreateAPIView):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer

class SingleClienteView(RetrieveUpdateDestroyAPIView):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer
