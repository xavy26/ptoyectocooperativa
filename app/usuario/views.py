from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from app.modelos.models import Usuario
from app.usuario.serializers import UsuarioSerializer

# Create your views here.

class UsuarioView(ListCreateAPIView):
    queryset = Usuario.objects.all()
    serializer_class = UsuarioSerializer

class SingleUsuarioView(RetrieveUpdateDestroyAPIView):
    queryset = Usuario.objects.all()
    serializer_class = UsuarioSerializer
