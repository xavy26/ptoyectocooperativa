from django.urls import path

from app.usuario.views import UsuarioView, SingleUsuarioView


app_name = "usuario"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    path('/', UsuarioView.as_view()),
    path('/<int:pk>', SingleUsuarioView.as_view()),
]
