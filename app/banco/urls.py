from django.urls import path

from app.banco.views import BancoView, SingleBancoView


app_name = "banco"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    path('/', BancoView.as_view()),
    path('/<int:pk>', SingleBancoView.as_view()),
]
