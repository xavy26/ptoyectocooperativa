from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from app.modelos.models import Banco
from app.banco.serializers import BancoSerializer

# Create your views here.

class BancoView(ListCreateAPIView):
    queryset = Banco.objects.all()
    serializer_class = BancoSerializer

class SingleBancoView(RetrieveUpdateDestroyAPIView):
    queryset = Banco.objects.all()
    serializer_class = BancoSerializer
