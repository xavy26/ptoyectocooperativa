from django.urls import path

from app.cuenta.views import CuentaView, SingleCuentaView


app_name = "cuenta"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    path('/', CuentaView.as_view()),
    path('/<int:pk>', SingleCuentaView.as_view()),
]
