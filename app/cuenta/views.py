from django.shortcuts import get_object_or_404
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from app.modelos.models import Cuenta, Cliente, Banco
from app.cuenta.serializers import CuentaSerializer

# Create your views here.

class CuentaView(ListCreateAPIView):
    queryset = Cuenta.objects.all()
    serializer_class = CuentaSerializer

    def perform_create(self, serializer):
        cliente = get_object_or_404(Cliente, cliente_id=self.request.data.get('cliente'))
        banco = get_object_or_404(Banco, banco_id=self.request.data.get('banco'))
        return serializer.save(cliente=cliente, banco=banco)

class SingleCuentaView(RetrieveUpdateDestroyAPIView):
    queryset = Cuenta.objects.all()
    serializer_class = CuentaSerializer
