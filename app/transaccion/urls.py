from django.urls import path

from app.transaccion.views import TransaccionView, SingleTransaccionView


app_name = "transaccion"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    path('/', TransaccionView.as_view()),
    path('/<int:pk>', SingleTransaccionView.as_view()),
]
