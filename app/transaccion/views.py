from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from app.modelos.models import Transaccion
from app.transaccion.serializers import TransaccionSerializer

# Create your views here.

class TransaccionView(ListCreateAPIView):
    queryset = Transaccion.objects.all()
    serializer_class = TransaccionSerializer

class SingleTransaccionView(RetrieveUpdateDestroyAPIView):
    queryset = Transaccion.objects.all()
    serializer_class = TransaccionSerializer
